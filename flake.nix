{
  description = "Nix Flake templates";

  outputs = {self}: {
    templates = {
      parts-web = {
        path = ./parts-nodejs;
        description = "flake-parts for Node.js projects";
      };
    };
  };
}
